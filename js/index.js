$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $("#modalReserva").on("show.bs.modal", function(e) {
        console.log('Modal abriendose..');
    })

    $("#modalReserva").on("shown.bs.modal", function(e) {
        console.log('Modal abierto..');
    })

    $("#modalReserva").on("hide.bs.modal", function(e) {
        console.log('Modal cerrandose...');
    })

    $("#modalReserva").on("hiden.bs.modal", function(e) {
        console.log('Modal cerrado...');
    })

    $("#modalReserva").on("show.bs.modal", function(e) {
        $("#reservaBtnBourbon").removeClass("btn-primary");
        $("#reservaBtnBourbon").addClass("btn-default");
        $("#reservaBtnBourbon").prop("disabled", true);
    })

    $("#modalReserva").on("hide.bs.modal", function(e) {
        $("#reservaBtnBourbon").removeClass("btn-default");
        $("#reservaBtnBourbon").addClass("btn-primary");
        $("#reservaBtnBourbon").prop("disabled", false);
    })
});